import java.lang.*;
import java.sql.*;

public class Demo {
    public static void main(String[] args){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cotxes","user1","pass1");
            Statement stmt = conn.createStatement();
            String insert = "INSERT INTO cotxe (nom, descripcio) VALUES ('VW Golf', 'Best car in the world')";
            stmt.executeUpdate(insert);
			stmt.close();
			conn.close();
        }catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception ex){
            System.out.println("Exception: " + ex.getMessage());
        }
    }
                                
}

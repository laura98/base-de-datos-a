package Main.actualitzar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ActualitzarProducteCodi {
    public static void main(String[] args) {

        String Cod = args[0], Nom =args[1];
        //String Cod = args[0];
        //String Nom = args[1];

        try {
            Class.forName("com.mysql.jdbc.Driver");// Cargar el driver
            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:mysql://localhost/HR", "usuari", "123456");

            String sql = String.format("UPDATE PRODUCTE SET DESCRIPCIO = %s WHERE PROD_NUM = %s",
                    Nom, Cod);

            System.out.println(sql);

            Statement sentencia = conexion.createStatement();
            int filas = sentencia.executeUpdate(sql);
            System.out.printf("Empleados modificados: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexión

        } catch (ClassNotFoundException cn) {
            cn.printStackTrace();
        } catch (SQLException e) {
            if (e.getErrorCode() == 1062)
                System.out.println("CLAVE PRIMARIA DUPLICADA");
            else
            if (e.getErrorCode() == 1452)
                System.out.println("CLAVE AJENA "+ Cod + " NO EXISTE");

            else {
                System.out.println("HA OCURRIDO UNA EXCEPCIÓN:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("Cód error:  " + e.getErrorCode());
            }
        }

    }// fin de main
}

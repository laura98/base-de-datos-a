package Main.insertar;

import CRUD.Producte_CRUD;
import Models.Producte;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InsertarProducte {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");// Cargar el driver
            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:mysql://localhost/HR", "usuari", "123456");


            // recuperar argumentos de main
            String Num = args[0]; // num. departamento
            String Desc = args[1]; // nombre

            //construir orden INSERT
            String sql = String.format("INSERT INTO PRODUCTE VALUES (%s, '%s')",
                    Num, Desc);

            System.out.println(sql);

            System.out.println(sql);
            Statement sentencia = conexion.createStatement();
            int filas = 0;
            try {
                filas = sentencia.executeUpdate(sql.toString());
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.printf("HA OCURRIDO UNA EXCEPCIÓN:%n");
                System.out.printf("Mensaje   : %s %n", e.getMessage());
                System.out.printf("SQL estado: %s %n", e.getSQLState());
                System.out.printf("Cód error : %s %n", e.getErrorCode());
            }


            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexión

        } catch (ClassNotFoundException cn) {
            cn.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package Main.insertar;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class InsertarEmpleats {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");// Cargar el driver
            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:mysql://localhost/HR", "usuari", "123456");


            // recuperar argumentos de main
            String numeroEmpleat=args[0], cap=args[1];
            String cognom=args[2], ofici=args[3];
            String dataAlta=args[4];
            String salari=args[5], comissio=args[6];
            String numeroDepartament=args[7];

            //construir orden INSERT
            String sql = String.format("INSERT INTO EMP VALUES (%s, '%s','%s','%s','%s','%s','%s','%s')",
                    numeroEmpleat,cognom,ofici,dataAlta,salari,comissio,numeroDepartament);

            System.out.println(sql);

            System.out.println(sql);
            Statement sentencia = conexion.createStatement();
            int filas = 0;
            try {
                filas = sentencia.executeUpdate(sql.toString());
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.printf("HA OCURRIDO UNA EXCEPCIÓN:%n");
                System.out.printf("Mensaje   : %s %n", e.getMessage());
                System.out.printf("SQL estado: %s %n", e.getSQLState());
                System.out.printf("Cód error : %s %n", e.getErrorCode());
            }


            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexión

        } catch (ClassNotFoundException cn) {
            cn.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package Models;

import java.util.Date;

public class Empleat {

    private short numeroEmpleat, cap;
    private String cognom, ofici;
    private Date dataAlta;
    private int salari, comissio;
    private byte numeroDepartament;

    public short getNumeroEmpleat() {
        return numeroEmpleat;
    }

    public void setNumeroEmpleat(short numeroEmpleat) {
        this.numeroEmpleat = numeroEmpleat;
    }

    public short getCap() {
        return cap;
    }

    public void setCap(short cap) {
        this.cap = cap;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public Date getDataAlta() {
        return dataAlta;
    }

    public void setDataAlta(Date dataAlta) {
        this.dataAlta = dataAlta;
    }

    public int getSalari() {
        return salari;
    }

    public void setSalari(int salari) {
        this.salari = salari;
    }

    public int getComissio() {
        return comissio;
    }

    public void setComissio(int comissio) {
        this.comissio = comissio;
    }

    public byte getNumeroDepartament() {
        return numeroDepartament;
    }

    public void setNumeroDepartament(byte numeroDepartament) {
        this.numeroDepartament = numeroDepartament;
    }

    public Empleat(short numeroEmpleat, short cap, String cognom, String ofici, Date dataAlta, int salari, int comissio, byte numeroDepartament) {
        this.numeroEmpleat = numeroEmpleat;
        this.cap = cap;
        this.cognom = cognom;
        this.ofici = ofici;
        this.dataAlta = dataAlta;
        this.salari = salari;
        this.comissio = comissio;
        this.numeroDepartament = numeroDepartament;
    }
}

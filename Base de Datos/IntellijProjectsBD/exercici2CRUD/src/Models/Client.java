package Models;

import java.math.BigDecimal;

public class Client {

    private int codiClient;
    private String nom, adreca, ciutat, estat, codiPostal, telefon, observacions;
    private short area, repr_cod;
    private BigDecimal limitCredit;

    public int getCodiClient() {
        return codiClient;
    }

    public void setCodiClient(int codiClient) {
        this.codiClient = codiClient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    public short getArea() {
        return area;
    }

    public void setArea(short area) {
        this.area = area;
    }

    public short getRepr_cod() {
        return repr_cod;
    }

    public void setRepr_cod(short repr_cod) {
        this.repr_cod = repr_cod;
    }

    public BigDecimal getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(BigDecimal limitCredit) {
        this.limitCredit = limitCredit;
    }

    public Client(int codiClient, String nom, String adreca, String ciutat, String estat, String codiPostal, String telefon, String observacions, short area, short repr_cod, BigDecimal limitCredit) {
        this.codiClient = codiClient;
        this.nom = nom;
        this.adreca = adreca;
        this.ciutat = ciutat;
        this.estat = estat;
        this.codiPostal = codiPostal;
        this.telefon = telefon;
        this.observacions = observacions;
        this.area = area;
        this.repr_cod = repr_cod;
        this.limitCredit = limitCredit;
    }
}

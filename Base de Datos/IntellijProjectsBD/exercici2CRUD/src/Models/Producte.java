package Models;

public class Producte {

    private int numeroProducte;
    private String descripcio;

    public int getNumeroProducte() {
        return numeroProducte;
    }

    public void setNumeroProducte(int numeroProducte) {
        this.numeroProducte = numeroProducte;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public Producte(int numeroProducte, String descripcio) {
        this.numeroProducte = numeroProducte;
        this.descripcio = descripcio;
    }
}

package CRUD;

import DB.BaseDeDatos;
import Models.Client;

import java.sql.*;

public class Client_CRUD {
    public void actualitzarCredit(Client... clients) {
        String query = "";
        BaseDeDatos bd = new BaseDeDatos();
        try {

            for (Client client: clients) {
                query = "UPDATE CLIENT SET LIMIT_CREDIT = " + client.getLimitCredit() +
                        " WHERE CLIENT_COD = " + client.getCodiClient(); // ` `string, sense no es string

                PreparedStatement sentenciaP = bd.conectar().prepareStatement(query);

                sentenciaP.executeUpdate();
                System.out.println("El registro se actualizó exitosamente.");
                sentenciaP.close();
            }
            bd.conexion.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}

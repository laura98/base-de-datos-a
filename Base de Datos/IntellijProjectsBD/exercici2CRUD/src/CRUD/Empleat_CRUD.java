package CRUD;

import DB.BaseDeDatos;
import Models.Empleat;

import java.sql.*;

public class Empleat_CRUD {
    public void agregarEmpleat(Empleat... empleats) throws SQLException {
        String query;
        BaseDeDatos bd = new BaseDeDatos();
        Statement sentencia = null;
        try {
            sentencia = bd.conectar().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Empleat empleat : empleats) {

            query = String.format("INSERT INTO EMP VALUES (%s, '%s', '%s', %s, '%s', %s, %s, %s)", // ` `string, sense no es string
                    empleat.getNumeroEmpleat(), empleat.getCognom(), empleat.getOfici(), empleat.getCap(), empleat.getDataAlta(),
                    empleat.getSalari(), empleat.getComissio(), empleat.getNumeroDepartament());
            if (sentencia != null) {
                if (sentencia.executeUpdate(query) > 0) {
                    System.out.println("El registro se insertó exitosamente.");
                } else {
                    System.out.println("No se pudo insertar el registro.");
                }
            }
            System.out.println(query);

        }
        if (sentencia != null) {
            sentencia.close();
        }
        bd.conexion.close();
    }

}

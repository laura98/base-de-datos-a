package CRUD;

import DB.BaseDeDatos;
import Models.Producte;

import java.sql.*;

    public class Producte_CRUD {



    public void agregarProducte(Producte... productes) throws SQLException {
        String query;
        BaseDeDatos bd = new BaseDeDatos();
        Statement sentencia = null;
        try {
            sentencia = bd.conectar().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Producte producte : productes) {

            query = String.format("INSERT INTO PRODUCTE VALUES (%s, '%s')", // ` `string, sense no es string
                    producte.getNumeroProducte(), producte.getDescripcio());
            if (sentencia != null) {
                if (sentencia.executeUpdate(query) > 0) {
                    System.out.println("El registro se insertó exitosamente.");
                } else {
                    System.out.println("No se pudo insertar el registro.");
                }
            }
            System.out.println(query);

        }
        if (sentencia != null) {
            sentencia.close();
        }
        bd.conexion.close();
    }

}// fin de la clase

package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDeDatos {

    private final String URL = "jdbc:mysql://localhost:3306/"; // Ubicación de la BD.
    private final String BD = "empresa"; // Nombre de la BD.
    private final String USER = "usuari";
    private final String PASSWORD = "123456";

    public static void instantiateDataBase(){

        Connection conn;

        {
            try {
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa",
                        "usuari","123456");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


    public Connection conexion = null;

    public Connection conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(URL + BD, USER, PASSWORD);
            if (conexion != null) {
                System.out.println("¡Conexión Exitosa!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return conexion;
        }
    }
}
